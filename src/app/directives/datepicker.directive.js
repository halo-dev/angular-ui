(function() {
   'use strict';

   angular
      .module('angularUi')
      .directive('datepickerPopup', datepickerPopup);

   /** @ngInject */
   function datepickerPopup() {
      var directive = {
         restrict: 'EAC',
         require: 'ngModel',
         link: function(scope, element, attr, controller) {
            controller.$formatters.shift();
         }
      };

      return directive;
   }

})();
