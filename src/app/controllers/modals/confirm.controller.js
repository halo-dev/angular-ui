(function() {
   'use strict';

   angular
      .module('angularUi')
      .controller('ConfirmModal.Controller', ConfirmCtrl);


   /** @ngInject */
   function ConfirmCtrl($modalInstance, data) {
      var vm = this;

      vm.cancel = cancel;
      vm.ok = ok;
      vm.properties = data;

      function cancel() {
         $modalInstance.dismiss();
      }

      function ok() {
         $modalInstance.close();
      }
   }
})();
