(function() {
   'use strict';

   angular
      .module('angularUi')
      .controller('Leagues.Controller', LeaguesController);


   /** @ngInject */
   function LeaguesController($modal, $log, initialData, apiService, dialogService) {
      var vm = this;

      vm.leagues = initialData;
      vm.currentEdit = {};
      vm.addItem = addItem;
      vm.removeItem = removeItem;
      vm.editItem = editItem;
      vm.cancelEditItem = cancelEditItem;
      vm.saveItem = saveItem;
      vm.hideAlert = hideAlert;
      vm.showAlert = true;

      function hideAlert() {
         vm.showAlert = false;
      }

      function addItem(name) {
         if(!name) return;
         apiService.addLeague(name).then(function (updatedLeaguesList) {
            vm.leagues = updatedLeaguesList;
         });
      }

      function removeItem(item) {
         dialogService.confirm( "Delete?",  "Are you sure you want to delete league : " + item.name).then(function() {
            apiService.deleteLeague(item.id).then(function (updatedLeaguesList) {
               vm.leagues = updatedLeaguesList;
            });
         },function() {
            $log.info("Modal dismissed ");
         });
      }

      function editItem(item) {
         vm.currentEdit[item.id] = true;
         vm.itemToEdit = angular.copy(item);
      }

      function cancelEditItem(id) {
         vm.currentEdit[id] = false;
      }

      function saveItem(item) {
         apiService.editLeague(item).then(function (updatedLeaguesList) {
            vm.leagues = updatedLeaguesList;
            vm.currentEdit[item.id] = false;
         });
      }
   }
})();
