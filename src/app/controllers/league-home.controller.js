(function() {
  'use strict';

  angular
    .module('angularUi')
    .controller('LeagueHome.Controller', LeagueHomeCtrl);


  /** @ngInject */
  function LeagueHomeCtrl($location, $routeParams, initialData) {
    var vm = this;

    vm.leagueHome = initialData;

    vm.go = go;

    function go(path) {
      $location.path('leagues/' + $routeParams.id + '/' + path);
    }
  }
})();
