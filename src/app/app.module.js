(function() {
  'use strict';

  angular
    .module('angularUi', ['ngAnimate', 'ngMessages', 'ngAria', 'ngResource', 'ngRoute', 'toastr', 'ui.bootstrap']);

})();
