(function() {
   'use strict';

   angular
      .module('angularUi')
      .factory('dialogService', dialogService);

   /** @ngInject */
   function dialogService($modal, constants) {
      var CONFIRM_DEFAULT_BUTTONS = ["OK", "Cancel"],
          VIEWS_URL = constants.getModalViewsURL();

      var service = {
         confirm : confirm
      };
      return service;

      function confirm(title, message, buttons) {
         var modalInstance = $modal.open({
            templateUrl: VIEWS_URL + "confirm-modal.html",
            controller: "ConfirmModal.Controller",
            controllerAs: "vm",
            resolve : {
               data: function() {
                  return {
                     title : title,
                     message : message,
                     buttons: buttons ? buttons : CONFIRM_DEFAULT_BUTTONS
                  }
               }
            },
            size: "sm"
         });

         return modalInstance.result;
      }

   }

})();
