(function() {
   'use strict';

   angular
      .module('angularUi')
      .factory('constants', constantsService);

   /** @ngInject */
   function constantsService() {
      var VIEW_URL = "views/modals/";

      var service = {
         getModalViewsURL : getModalViewsURL
      };
      return service;

      function getModalViewsURL() {
         return VIEW_URL;
      }
   }

})();
