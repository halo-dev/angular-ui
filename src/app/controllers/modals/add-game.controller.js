/* global moment :false */
(function() {
   'use strict';

   angular
      .module('angularUi')
      .controller('NewGameModal.Controller', addGameCtrl);


   /** @ngInject */
   function addGameCtrl($modalInstance, data) {
      var vm = this;

      vm.cancel = cancel;
      vm.add = add;
      vm.opened = false;
      vm.open = openDatePicker;
      vm.isEdit = !!data.game;
      vm.game = vm.isEdit ? data.game : {};
      vm.teamList = data.teamList ? data.teamList : [];

      init();
      function init() {
         vm.game.date = moment();
         vm.game.time = moment();
      }

      function cancel() {
         $modalInstance.dismiss();
      }

      function openDatePicker($event) {
         $event.preventDefault();
         $event.stopPropagation();
         vm.opened = true;
      }

      function add($event) {
         $event.preventDefault();
         $modalInstance.close(vm.game);
      }
   }
})();
