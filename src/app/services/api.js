/* global _:false */
(function() {
   'use strict';

   angular
      .module('angularUi')
      .factory('apiService', apiService);

   /** @ngInject */
   function apiService($http, $q) {
      var ID= "id",
          myLeaguesURL = "https://api.myjson.com/bins/56y6l",
          myGamesURL = "https://api.myjson.com/bins/3n12d",
          myHomeLeagueURL = "https://api.myjson.com/bins/1z0kl",
          myTeamsURL = "https://api.myjson.com/bins/1o31x";

      var service = {
         getLeagues : getLeagues,
         addLeague: addLeague,
         editLeague: editLeague,
         deleteLeague: deleteLeague,
         getTeamsByLeague: getTeamsByLeague,
         addTeam: addTeam,
         editTeam: editTeam,
         deleteTeam: deleteTeam,
         getGamesInfo : getGamesInfo,
         getGamesByLeague: getGamesByLeague,
         addGame: addGame,
         editGame: editGame,
         deleteGame : deleteGame,
         getHomeLeague: getHomeLeague
      };
      return service;

      //helpers
      function getJSON(URL) {
         return $http({
            method: 'GET',
            url: URL
         }).then(function(data) {
            return data.data;
         });
      }

      function updateJSON(URL, data) {
         return $http.put(URL , JSON.stringify(data)).then(function (data) {
            return data.data;
         });
      }

      // LEAGUES
      function getLeagues() {
         return getJSON(myLeaguesURL);
      }

      function saveLeagues(leagues) {
         return updateJSON(myLeaguesURL, leagues);
      }

      function addLeague(name) {
         return getLeagues().then(function(leaguesList) {
            var newLeagueList = leaguesList.length ? leaguesList : [],
                newLeague = {
                   name: name,
                   id: _.last(leaguesList) ? _.last(leaguesList)[ID] + 1 : 0
                };
            newLeagueList.push(newLeague);
            return saveLeagues(newLeagueList);
         });
      }

      function editLeague(league) {
         return getLeagues().then(function (leaguesList) {
            _.extend(_.findWhere(leaguesList, { id: league.id }), league);
            return saveLeagues(leaguesList);
         })
      }

      function deleteLeague(leagueID) {
         return getLeagues().then(function (leaguesList) {
            leaguesList = _.without(leaguesList, _.findWhere(leaguesList, { id: leagueID}));
            return saveLeagues(leaguesList);
         });
      }

      // TEAMS
      function getTeams() {
         return getJSON(myTeamsURL);
      }

      function saveTeams(teams) {
         return updateJSON(myTeamsURL, teams);
      }

      function getTeamsByLeague(leagueID) {
         return getTeams().then(function(teams) {
            return _.where(teams, {leagueID : leagueID});
         });
      }

      function addTeam(team) {
         return getTeams().then(function(teams) {
            var newTeamsList = teams.length ? teams : [];

            team.id =  _.last(teams) ? _.last(teams)[ID] + 1 : 0;
            newTeamsList.push(team);
            return saveTeams(newTeamsList);
         });
      }

      function editTeam(team) {
         return getTeams().then(function(teams) {
            _.extend(_.findWhere(teams, {id : team.id}), team);
            return saveTeams(teams);
         });
      }

      function deleteTeam(teamID) {
         return getTeams().then(function(teams) {
            teams = _.without(teams, _.findWhere(teams, { id : teamID }));
            return saveTeams(teams);
         });
      }

      //GAMES

      function getGamesInfo(leagueID) {
         return $q.all([
            getTeams(),
            getGamesByLeague(leagueID)
         ]).then(function(data) {
            return {
               games : data[1],
               teams : data[0]
            }
         })
      }

      function getGames() {
         return getJSON(myGamesURL);
      }

      function saveGames(games) {
         return updateJSON(myGamesURL, games);
      }

      function getGamesByLeague(leagueID) {
         return getGames().then(function(games) {
            return _.where(games, {leagueID : leagueID});
         });
      }

      function addGame(game) {
         return getGames().then(function(games) {
            var newGamesList = games.length ? games : [];

            game.id = _.last(games) ? _.last(games)[ID] + 1 : 0;
            newGamesList.push(game);
            return saveGames(newGamesList);
         });
      }

      function editGame(game) {
         return getGames().then(function(games) {
            _.extend(_.findWhere(games, {id : game.id}), game);
            return saveGames(games);
         });
      }

      function deleteGame(gameID) {
         return getGames().then(function(games) {
            games = _.without(games, _.findWhere(games, {id : gameID}));
            return saveGames(games);
         });
      }

      // HOME LEAGUES

      function getHomeLeague() {
         return $http({
            method: 'GET',
            url: myHomeLeagueURL
         }).then(function(data) {
            return data.data;
         });
      }
   }

})();
