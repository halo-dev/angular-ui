(function() {
   'use strict';

   angular
      .module('angularUi')
      .controller('NewTeamModal.Controller', addTeamCtrl);


   /** @ngInject */
   function addTeamCtrl($modalInstance, data) {
      var vm = this;

      vm.cancel = cancel;
      vm.add = add;
      vm.isEdit = !!data.team;
      vm.team = vm.isEdit ? data.team : {};
      vm.divisionsList = data.divisionsList ? data.divisionsList : [];

      function cancel() {
         $modalInstance.dismiss();
      }

      function add($event) {
         $event.preventDefault();
         var newTeam = vm.team;
         $modalInstance.close(newTeam);
      }
   }
})();
