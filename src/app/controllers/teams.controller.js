/* global _:false */
(function() {
   'use strict';

   angular
      .module('angularUi')
      .controller('Teams.Controller', TeamsCtrl);


   /** @ngInject */
   function TeamsCtrl($log, $location, $routeParams, $modal,
                      dialogService, apiService, constants, initialData) {
      var vm = this,
          leagueID;

      vm.teams = initialData;

      vm.go = go;
      vm.addTeam = addTeam;
      vm.deleteTeam = deleteTeam;
      vm.expand = expand;
      vm.editTeam = editTeam;

      init();

      function init() {
         leagueID = $routeParams.id;
         updateGroups(true);
      }

      function go(path) {
         $location.path('leagues/' + leagueID + '/' + path);
      }

      function updateGroups(initialSetUp) {
         if(!vm.teams.length) return
         vm.groups = _.chain(vm.teams)
                     .groupBy("divisionName")
                     .pairs()
                     .map(function(item) {
                        return {
                           divisionName : item[0],
                           teams : item[1],
                           isOpen : initialSetUp ? initialSetUp : false
                        };
                     })
                     .sortBy("divisionName")
                     .value();
      }

      function expand(show) {
         _.each(vm.groups, function(divisions) {
            divisions.isOpen = show;
         })
      }

      function addTeam() {
         var modalInstance = $modal.open({
            templateUrl: constants.getModalViewsURL() + "new-team.html",
            controller: "NewTeamModal.Controller",
            controllerAs: "vm",
            resolve : {
               data: function() {
                  return {
                     divisionsList : _.chain(vm.teams).uniq("divisionName").map("divisionName").value()
                  };
               }
            },
            size: "lg"
         });

         modalInstance.result.then(function(team) {
            team.leagueID = leagueID;
            apiService.addTeam(team).then(function(teams) {
               vm.teams = teams;
               updateGroups(true);
            });
         });
      }

      function deleteTeam(team) {
         dialogService.confirm("Delete", "Are you sure you want to delete team " + team.name + "?").then(function() {
            apiService.deleteTeam(team.id).then(function(teams) {
               vm.teams = teams;
               updateGroups();
            });
         });
      }

      function editTeam(team) {
         var modalInstance = $modal.open({
            templateUrl: constants.getModalViewsURL() + "new-team.html",
            controller: "NewTeamModal.Controller",
            controllerAs: "vm",
            resolve : {
               data: function() {
                  return {
                     team : angular.copy(team),
                     divisionsList : _.chain(vm.teams).uniq("divisionName").map("divisionName").value()
                  }
               }
            },
            size: "lg"
         });

         modalInstance.result.then(function(team) {
            apiService.editTeam(team).then(function(teams) {
               vm.teams = teams;
               updateGroups(true);
            });
         });
      }
   }
})();
