/* global _:false, moment:false */
(function() {
   'use strict';

   angular
      .module('angularUi')
      .controller('Games.Controller', GamesCtrl);


   /** @ngInject */
   function GamesCtrl($modal, $location, $routeParams, apiService, constants, dialogService, initialData) {
      var vm = this,
          leagueID;

      vm.games = initialData.games;
      vm.teams = initialData.teams;

      vm.go = go;
      vm.addGame = addGame;
      vm.deleteGame = deleteGame;
      vm.combineDates = combineDates;
      vm.editGame = editGame;

      function go(path) {
         $location.path('leagues/' + $routeParams.id + '/' + path);
      }

      init();
      function init() {
         leagueID = $routeParams.id;
      }

      function addGame() {
         var modalInstance = $modal.open({
            templateUrl: constants.getModalViewsURL() + "new-game.html",
            controller: "NewGameModal.Controller",
            controllerAs: "vm",
            resolve : {
               data: function() {
                  return {
                     teamList :  _.chain(vm.teams).uniq("name").map("name").value()
                  }
               }
            },
            size: "lg"
         });

         modalInstance.result.then(function(game) {
            game.leagueID = leagueID;
            apiService.addGame(game).then(function(games) {
               vm.games = games;
            });
         });
      }

      function combineDates(date, time) {
         var dateString = moment(date).format("MM/DD/YYYY") + " " + moment(time).format('HH:mm');
         return dateString;
      }

      function deleteGame(game) {
         dialogService.confirm("Delete", "Are you sure you want to delete  " + game.homeTeam + " vs " + game.awayTeam).then(function() {
            apiService.deleteGame(game.id).then(function(games) {
               vm.games = games;
            });
         });
      }

      function editGame(game) {
         var modalInstance = $modal.open({
            templateUrl: constants.getModalViewsURL() + "new-game.html",
            controller: "NewGameModal.Controller",
            controllerAs: "vm",
            resolve : {
               data: function() {
                  return {
                     teamList :  _.chain(vm.teams).uniq("name").map("name").value(),
                     game : game
                  }
               }
            },
            size: "lg"
         });

         modalInstance.result.then(function(game) {
            apiService.editGame(game).then(function(games) {
               vm.games = games;
            });
         });
      }
   }
})();
