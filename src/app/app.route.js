(function () {
   'use strict';

   angular
      .module('angularUi')
      .config(routeConfig);

   /** @ngInject */
   function routeConfig($routeProvider) {
      $routeProvider.when('/', {
         templateUrl: 'views/main.html',
         controller: 'Main.Controller',
         controllerAs: 'main'
      }).when('/leagues', {
         templateUrl: 'views/leagues.html',
         controller: 'Leagues.Controller',
         controllerAs: 'vm',
         resolve: {
            initialData : ['apiService', function (apiService) {
               return apiService.getLeagues();
            }]
         }
      }).when('/leagues/:id/teams', {
         templateUrl: 'views/teams.html',
         controller: 'Teams.Controller',
         controllerAs: 'vm',
         resolve: {
            initialData : ['$route', 'apiService', function ($route, apiService) {
               return apiService.getTeamsByLeague($route.current.params.id);
            }]
         }
      }).when('/leagues/:id/games', {
         templateUrl: 'views/games.html',
         controller: 'Games.Controller',
         controllerAs: 'vm',
         resolve: {
            initialData : ['$route', 'apiService', function ($route, apiService) {
               return apiService.getGamesInfo($route.current.params.id);
            }]
         }
      }).when('/leagues/:id/league-home', {
         templateUrl: 'views/league-home.html',
         controller: 'LeagueHome.Controller',
         controllerAs: 'vm',
         resolve: {
            initialData : ['$route', 'apiService', function ($route, apiService) {
               return apiService.getHomeLeague($route.current.params.id);
            }]
         }
      }).otherwise({
         redirectTo: '/'
      });
   }

})();
